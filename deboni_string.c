#include<stdio.h>
#include "deboni_string.h"

int
main(int arg_count, char **arg_vector)
{
    u8 buffer[256];
    DString string = {
        .len = 0,
        .capacity = 256,
        .ptr = buffer,
    };
    
    build_string_from_literal(&string, "batatinha");
    printf("sting len = %d\n", string.len);
    printf("sting capacity = %d\n", string.capacity);
    
    print_dstring_ln(string);
    
    printf("\nAppending yay\n");
    build_string_append_literal(&string, ", yay");
    print_dstring_ln(string);
    
    DString string2 = string;
    printf("\nAppending yay until error\n");
    
    while (!build_string_append_literal(&string2, ", yay"));
    printf("\nString\n");
    print_dstring_ln(string);
    
    printf("\nString 2\n");
    print_dstring_ln(string2);
    
    string.len = string.capacity;
    printf("\nString\n");
    print_dstring_ln(string);
    
    string.len = 16;
    string2.len = 0;
    u8 buffer2[256];
    string2.ptr = buffer2;
    
    printf("\nString\n");
    print_dstring_ln(string);
    
    build_string_copy(&string2, string);
    
    printf("\nString 2\n");
    print_dstring_ln(string2);
    
    build_string_append(&string2, string);
    
    build_string_from_literal(&string2, "yay batata potato");
    printf("\nString 2\n");
    print_dstring_ln(string2);
    
    DString tok;
    do {
        tok = next_token(&string2, ' ');
        print_dstring_ln(tok);
    } while (string2.len > 0);
    
    build_string_from_literal(&string2, "yay batata potato yay");
    SubString sub_string = substring(string2, literal_to_string("batata"));
    
    printf("Substring = [%d : %d]\n", sub_string.offset, sub_string.len);
    
    DString string3 = string_between(string2,
                                     literal_to_string("batata"),
                                     literal_to_string("yay"));
    print_dstring_ln(string3);
    printf("s3 == 'potato' = %s\n", 
           dstring_comp(string3 , literal_to_string("potato")) ? "true" : "false");
    
    remove_trailing_spaces(&string3);
    print_dstring_ln(string3);
    printf("s3 == 'potato' = %s\n",
           dstring_comp(string3 , literal_to_string("potato")) ? "true" : "false");
    
    remove_leading_spaces(&string3);
    print_dstring_ln(string3);
    printf("s3 == 'potato' = %s\n",
           dstring_comp(string3 , literal_to_string("potato")) ? "true" : "false");
    
}