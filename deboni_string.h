/* date = October 29th 2020 3:19 pm */

#ifndef DEBONI_STRING_H
#define DEBONI_STRING_H

#include<stdint.h>
#include<stdlib.h>

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef uint8_t  i8;
typedef uint16_t i16;
typedef uint32_t i32;
typedef uint64_t i64;

typedef float  f32;
typedef double f64;

typedef uint32_t b32;

#define true  1;
#define false 0;

#ifndef NOASSERT
#define ASSERT(x) \
if(!(x)) { \
printf("Assert failed %d:%s\n", __LINE__, __FILE__); \
exit(42); \
}
#else
#define ASSERT(x)
#endif

#define For(s) for (u32 i = 0, it = s.ptr[0]; \
i < s.len; \
it = s.ptr[++i])

// ===== Definitions =====
typedef struct DString
{
    u32 len;
    u32 capacity;
    u8 *ptr;
} DString;

typedef enum BuildStringError
{
    BuildStringError_None = 0,
    BuildStringError_StringFull = 1,
    BuildStringError_NotEnoughSpace = 2,
} BuildStringError;

typedef struct SubString
{
    u32 offset;
    u32 len;
} SubString;

// ===== Functions =====

static BuildStringError
build_string_from_literal(DString *s, char *ls)
{
    s->len = 0;
    for (int i = 0; *ls != 0; i++, ls++) {
        s->len++;
        s->ptr[i] = *ls;
        
        if (i >= s->capacity) {
            return BuildStringError_StringFull;
        }
    }
    
    return BuildStringError_None;
}

static BuildStringError
build_string_append_literal(DString *s, char *ls)
{
    for (int i = s->len; *ls != 0; i++, ls++) {
        s->len++;
        s->ptr[i] = *ls;
        
        if (i >= s->capacity) {
            return BuildStringError_StringFull;
        }
    }
    
    return BuildStringError_None;
}

static void
build_string_append(DString *s1, DString s2)
{
    ASSERT(s1->capacity >= (s2.len + s1->len));
    
    For(s2) {
        s1->ptr[i + s1->len] = it;
    }
    s1->len += s2.len;
}

static void
build_string_copy(DString *s1, DString s2) {
    ASSERT(s1->capacity >= s2.len);
    
    s1->len = s2.len;
    For(s2) {
        s1->ptr[i] = it;
    }
}

static void
print_dstring(DString s)
{
    For(s) putchar(it);
}

static void
print_dstring_ln(DString s)
{
    For(s) putchar(it);
    putchar('\n');
}

static DString
next_token(DString *s, u8 separator)
{
    DString result = *s;
    
    result.len = 0;
    u32 max_len = s->len;
    while ((result.len < max_len) && (*(s->ptr) != separator)) {
        result.len++;
        s->capacity--;
        s->len--;
        s->ptr++;
    }
    
    if (s->len != 0) {
        s->ptr++;
        s->len--;
        s->capacity--;
    }
    
    return result;
}

// NOTE(Samuel): Works with both CRLF and LF
static DString
get_line(DString *s)
{
    DString line = next_token(s, '\n');
    if (line.ptr[line.len-1] == '\r') line.len--;
    return line;
}

// NOTE(Samuel): Return a DString using the memory of a string literal
// You cannot chant its content, i think
static DString
literal_to_string(char *sl)
{
    DString result = {0};
    result.ptr = (u8 *)sl;
    while (*(sl++) != 0) {
        result.len++;
        result.capacity++;
    }
    return result;
}

static SubString
substring(DString s, DString sub)
{
    SubString result = {0};
    
    u32 i;
    for (i = 0; i < (s.len - sub.len + 1); i++) {
        if (s.ptr[i] == sub.ptr[0]) {
            b32 found = true;
            for (u32 j = 0; j < sub.len; j++) {
                if (s.ptr[i + j] != sub.ptr[j]) {
                    found = false;
                    break;
                }
            }
            if (found) {
                result.offset = i;
                result.len = sub.len;
                break;
            } 
        }
    }
    
    return result;
}

static inline void
offset_string(DString *s, i32 offset)
{
    s->ptr += offset;
    s->len -= offset;
    s->capacity -= offset;
}

static DString
string_between(DString s, DString first, DString second)
{
    DString result = s;
    
    SubString sub = substring(s, first);
    offset_string(&result, (i32)(sub.offset + sub.len));
    sub = substring(result, second);
    
    result.len = sub.offset;
    result.capacity = sub.offset;
    
    return result;
}

static void
remove_trailing_spaces(DString *s)
{
    while (s->ptr[s->len - 1] == ' ' && s->len > 0) {
        s->len--;
    }
}

static void
remove_leading_spaces(DString *s)
{
    while (s->ptr[0] == ' ' && s->len > 0) {
        offset_string(s, 1);
    }
}

static b32
dstring_comp(DString s1, DString s2)
{
    if (s1.len != s2.len) {
        return false;
    }
    
    if (s1.ptr == s2.ptr) {
        return true;
    }
    
    For(s1) {
        if (it != s2.ptr[i]) {
            return false;
        } 
    }
    
    return true;
}

#endif //DEBONI_STRING_H
