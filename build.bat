@echo off

set opts=-FC -GR- -EHa- -nologo -Zi
set code=%cd%
pushd bin
cl %opts% %code%\deboni_string.c -Fedeboni_string_demo
popd
