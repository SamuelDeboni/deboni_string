#!/bin/bash

code="$PWD"
opts=-g
[[ -d bin ]] || mkdir bin
cd bin > /dev/null
clang $opts $code/deboni_string.c -o deboni_string_demo
cd $code > /dev/null
